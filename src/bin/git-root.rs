use std::{
    env,
    error::Error
};
use git2::Repository;


fn main() -> Result<(), Box<dyn Error>> {
    let pwd = env::current_dir()?;
    let repository = Repository::discover(pwd)?;
    let workdir = repository.workdir().ok_or("NoError")?;
    println!("{}", workdir.display());
    Ok(())
}